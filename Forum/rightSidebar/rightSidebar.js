import { Helpers } from '../utils/helpers.js';

export class RightSidebar {
	constructor() {
		this.uniqueMembers = 0;
		this.uniqueQuestions = 0;
	}

	setQuestionsAndMemberStats(membersCount, questionsCount) {
		this.uniqueMembers = membersCount;
		this.uniqueQuestions = questionsCount;
		this.renderMembersAndQuestionsCount();
	}

	renderMembersAndQuestionsCount() {
		const questionsCountDiv = document.querySelectorAll('.rs__stats-questions')[0];
		const questionsMembersDiv = document.querySelectorAll('.rs__stats-members')[0];

		Helpers.createElement('p', questionsMembersDiv, this.uniqueMembers, 'rs__stats-amount');
		Helpers.createElement('p', questionsCountDiv, this.uniqueQuestions, 'rs__stats-amount');
	}
}