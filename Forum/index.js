import { QuestionsList } from './questionList/questionsList.js';
import { RightSidebar } from '../rightSidebar/rightSidebar.js';
import { QuestionCreateEditForm } from './createEditQuestionForm/questionCreateEditForm.js';

const app = () => {
	const questionId = new URL(window.location.href).searchParams.get('id');
	const pathName = window.location.pathname;

	const questionsList = new QuestionsList();
	const rightSidebar = new RightSidebar();
	const questionCreateEditForm = new QuestionCreateEditForm();

	if (!questionId && (pathName === '/index.html' || pathName === '/')) {
		questionsList.init(rightSidebar);

		return;
	}

	questionCreateEditForm.init(questionId, questionsList);
};

document.addEventListener('DOMContentLoaded', () => {
	app();
});
