import { Modal } from '../utils/modal.js';
import { Rest } from '../utils/rest.js';
import * as CONSTANTS from '../utils/constants.js';

export class QuestionCreateEditForm {
	init(id, questions) {
		if (!id) {
			const formPostButton = document.querySelector('#postButton');
			formPostButton.addEventListener('click', () => this.modifyQuestion(id));

			return;
		}

		const formEditButton = document.querySelector('#editButton');
		const formDeleteButton = document.querySelector('#deleteButton');

		formEditButton.addEventListener('click', () => this.modifyQuestion(id));
		formDeleteButton.addEventListener('click', e => Modal.openModal(e, id, questions));

		this.getQuestionWithId(id);
	}

	deletePost(id) {
		Rest.delete(`questions/${ id }`).then(() => (window.location = '/index.html'));
	}

	getFormElementValue(id) {
		return document.getElementById(id).value;
	}

	getQuestionWithId(id) {
		Rest.get(`questions/${ id }`).then(this.fillEditForm.bind(this));
	}

	getFormData() {
		return {
			title: this.getFormElementValue('post-title'),
			description: this.getFormElementValue('post-description'),
			keywords: this.getFormElementValue('keywords').split(' '),
			category: this.getFormElementValue('category'),
			createdAt: this.getFormElementValue('creation-date'),
			author: {
				name: this.getFormElementValue('author-name'),
				occupation: this.getFormElementValue('author-occupation'),
				imgUrl: this.getFormElementValue('author-imgurl')
			},
			stats: {
				views: 15,
				answers: 2,
				votes: 85
			}
		};
	}

	assignValueToFormElement(id, value) {
		document.getElementById(id).value = value;
	}

	fillEditForm(data) {
		this.assignValueToFormElement('post-title', data.title);
		this.assignValueToFormElement('post-description', data.description);
		this.assignValueToFormElement('author-name', data.author.name);
		this.assignValueToFormElement('author-occupation', data.author.occupation);
		this.assignValueToFormElement('author-imgurl', data.author.imgUrl);
		this.assignValueToFormElement('keywords', data.keywords.join(' '));
		this.assignValueToFormElement('category', data.category);
		this.assignValueToFormElement('creation-date', data.createdAt);
	}

	modifyQuestion(id) {
		const form = document.querySelector('.post-form');
		const formData = this.getFormData();

		if (form.checkValidity()) {
			if(id) {
				Rest.put(`questions/${ id }`, formData).then(() => (window.location = CONSTANTS.INDEX_URL));
				return;
			}

			Rest.post('questions', formData).then(() => (window.location = CONSTANTS.INDEX_URL));
		}
	}
}