import * as CONSTANTS from './constants.js';

export class Rest {
	static post(url, data) {
		return fetch(CONSTANTS.BACKEND_URI + url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json; charset=utf-8'
			},
			body: JSON.stringify(data)
		});
	}

	static put(url, data) {
		return fetch(CONSTANTS.BACKEND_URI + url, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json; charset=utf-8'
			},
			body: JSON.stringify(data)
		});
	}

	static get(url) {
		return fetch(CONSTANTS.BACKEND_URI + url)
			.then(response => response.json())
			.catch(err => {
				throw err;
			});
	}

	static delete(url) {
		return fetch(CONSTANTS.BACKEND_URI + url, {
			method: 'DELETE'
		})
			.then(response => response.json());
	}
}