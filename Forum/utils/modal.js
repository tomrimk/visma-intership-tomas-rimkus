export class Modal {
	static openModal(e, id, questions) {
		e.preventDefault();

		const deleteModal = document.getElementById('toggle-modal-visibility');
		deleteModal.classList.add('visibility');

		const blurContainer = document.getElementById('blur-container');
		blurContainer.classList.add('on-modal-blur');

		const deleteAction = document.querySelector('.delete-confirm');
		const cancelModal = document.querySelector('.delete-cancel');

		cancelModal.addEventListener('click', () => this.closeModal(deleteModal, blurContainer));
		deleteAction.addEventListener('click', () => questions.deletePost(id));
	}

	static closeModal(modal, blurContainer) {
		modal.classList.remove('visibility');
		blurContainer.classList.remove('on-modal-blur');
	}
}