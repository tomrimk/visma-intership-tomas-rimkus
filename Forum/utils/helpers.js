export class Helpers {
	static createElement(element, parent, text, classList) {
		const newElement = document.createElement(element);

		if (classList && classList.length) {
			newElement.classList = classList;
		}

		if (text) {
			const elementText = document.createTextNode(text);
			newElement.appendChild(elementText);
		}

		parent.appendChild(newElement);
		return newElement;
	}
}