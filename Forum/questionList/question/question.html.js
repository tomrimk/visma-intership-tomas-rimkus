import * as CONSTANTS from '../../utils/constants.js';

export const questionTemplate = question => {
	return `<div class="questions__post questions__grid-item">

    <div class="questions__post-details">
        <a 
            href='${ CONSTANTS.FRONTEND_URI }createEditQuestionForm/editQuestion.html?id=${ question.id }' 
            class="questions__post-title word-break-ellipsis">
            ${ question.title }
        </a>
        <p class="questions__post-description word-break-ellipsis">${ question.description }</p>

        <div class="questions__post-keywords keyword${ question.id }">
        </div>

        <div class="questions__post-author">
            <img class="questions__post-author-image" src=${ question.author.imgUrl } alt="Marry">
            <p class="questions__post-author-name">${ question.author.name }</p>
            <div class="questions__post-author-occupation" occupation=${ question.author.occupation.toLowerCase() }>
                ${ question.author.occupation }
            </div>
            <p class="questions__post-add-time">Asked on ${ question.createdAt } in
                <a href="#" class="questions__post-category">${ question.category }</a>. 
            </p>
        </div>
    </div>

    <div class="questions__post-stats-wrapper">
        <div>
            <div class="questions__post-stat">${ question.stats.views }</div>
            <div class="questions__post-stat">${ question.stats.answers }</div>
            <div class="questions__post-stat">${ question.stats.votes }</div>
        </div>

        <div>
            <p class="questions__post-stat-label">views</p>
            <p class="questions__post-stat-label">answers</p>
            <p class="questions__post-stat-label">votes</p>
        </div>
    </div>
</div>
`;
};

