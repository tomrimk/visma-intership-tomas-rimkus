import * as templates from './question.html.js';
import { Helpers } from '../../utils/helpers.js';

export class Question {
	constructor(data) {
		this.data = data;
		this.renderQuestionHTML();
		this.renderKeywords();
	}

	renderQuestionHTML() {
		const questionsContainer = document.querySelector('#questions-list-container');
		questionsContainer.insertAdjacentHTML('afterbegin', templates.questionTemplate(this.data));
	}

	renderKeywords() {
		this.data.keywords.forEach(keyword => {
			const keywordsNav = document.querySelector(`.keyword${ this.data.id }`);
			const keywordDiv = Helpers.createElement('div', keywordsNav, keyword);
			keywordsNav.appendChild(keywordDiv);
		});
	}
}