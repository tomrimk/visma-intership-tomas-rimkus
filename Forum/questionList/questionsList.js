import { Rest } from '../utils/rest.js';
import { Question } from './question/question.js';
import { questionsList } from './questionsList.html.js';

export class QuestionsList {
	constructor() {
		this.uniqueMemberList = [];
		this.listedQuestions = [];
		this.rightSidebar = {};
	}

	init(rightSidebar) {
		this.createQuestionsListContainer();
		this.rightSidebar = rightSidebar;
		this.fetchQuestions();
	}

	createQuestionsListContainer() {
		const questionsListContainer = document.querySelector('#questions-container');

		questionsListContainer.insertAdjacentHTML('beforeend', questionsList());
	}

	fetchQuestions() {
		Rest.get('questions').then((questions) => {
			this.populateQuestions(questions);
			this.rightSidebar.setQuestionsAndMemberStats(this.uniqueMemberList.length, this.listedQuestions.length);
		});
	}

	populateQuestions(questions) {
		questions.forEach(question => {
			const newQuestion = new Question(question);

			this.listedQuestions = [...this.listedQuestions, newQuestion];

			const isMemberCounted = this.uniqueMemberList.find(member => member === question.author.name);

			if (!isMemberCounted) {
				this.uniqueMemberList = [...this.uniqueMemberList, question.author.name];
			}
		});
	}
}