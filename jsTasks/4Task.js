// Create two versions of a function called sum which takes a list of numbers and returns a sum of them. 
// a) Do not use any ES6 methods 
// b) Use a suitable ES6 method

let listOfNumbers = [15, 88, 12, 33, 22, 13, 21];

function sumNotES6(list) {
    let sum = 0;
    list.forEach(element => {
        sum += element;
    });

    return sum;
}

const sumES6 = list => list.reduce((acc, val) => acc + val);

console.log("Sum without ES6:", sumNotES6(listOfNumbers));
console.log("Sum with ES6:", sumES6(listOfNumbers));