// Write a loop that makes seven calls to console.log to output the following triangle: 
// # 
// ## 
// ### 
// #### 
// ##### 
// ###### 
// ####### 
// Print Every line in random color every time (choose from colors red, green, yellow, blue). There should not be two lines of the same color next to each other. 

const colors = require('colors/safe');

let hashtagColors = [colors.red, colors.green, colors.yellow, colors.blue];

let randomColor = hashtagColors[Math.floor(Math.random() * hashtagColors.length)];

const changeRandomColor = () => {
  let newRandomColor = hashtagColors[Math.floor(Math.random() * hashtagColors.length)];

  if (newRandomColor === randomColor) {
    return changeRandomColor();
  }

  randomColor = newRandomColor
}

for (let i = 0; i <= 7; i++) {
  for (let j = 0; j < i; j++) {
    process.stdout.write(randomColor('#'));
  }
  changeRandomColor();
  console.log();
}