// Given a list of the following major Houses of Westeros and their respective mottos Write two versions of a function that, when passed the name of a House, returns its motto. 
// a) Do not use any ES6 methods 
// b) Use a suitable ES6 method

var houses = [
    { name: "Targaryen", motto: "Fire and Blood" },
    { name: "Stark", motto: "Winter is Coming" },
    { name: "Bolton", motto: "Our Blades Are Sharp" },
    { name: "Greyjoy", motto: "We Do Not Sow" },
    { name: "Tully", motto: "Family, Duty, Honor" },
    { name: "Arryn", motto: "As High as Honor" },
    { name: "Lannister", motto: "Hear Me Roar!" },
    { name: "Tyrell", motto: "Growing Strong" },
    { name: "Baratheon", motto: "Ours is the Fury" },
    { name: "Martell", motto: "Unbowed, Unbent, Unbroken" }
];

function mottoNotES6(house) {
    let foundHouse = houses.filter(function (item) {
        return item.name === house
    })[0];

    if (foundHouse) {
        console.log(foundHouse.motto);
    } else {
        console.log('House with a name of ' + house + ' was not found');
    }
}

const mottoES6 = (house) => {
    let foundHouse = houses.find(item => item.name === house);

    if (foundHouse) {
        console.log(foundHouse.motto);
    } else {
        console.log('House with a name of ' + house + ' was not found');
    }
};

mottoNotES6('Lannister');
mottoES6('Stark');