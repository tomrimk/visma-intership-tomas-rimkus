// Create a function wordSearch(w) that searches to see whether a word w is present in the given text variable. Please note it has to be a full word. 

let paragraph = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

const wordSearch = (w) => {
    let searchedWord = paragraph.replace(/[.,]/g, '').split(' ').find(item => item === w);

    if (searchedWord) {
        console.log(`The word '${w}' is located on index: ${paragraph.indexOf(w)}`);
    } else {
        console.log(`Word '${w}' was not found in given text`);
    }
}

wordSearch('nulla');