// Create a simple function wrapper that will log every call to the wrapped function. 
// Example: 
// let spied = spy(myFunction); 
// spied(1); 
// let report = spied.report(); returns { totalCalls: 1 } 

let myFunction = function (str) {
    console.log(str);
}

let spy = function (passedFunction) {
    let totalCalls = 0;

    function wrappedFunction() {
        let args = Array.prototype.slice.call(arguments)[0];
        totalCalls += 1;
        return passedFunction(args);
    }

    wrappedFunction.report = function () {
        return { totalCalls };
    }

    return wrappedFunction;
}

let spied = spy(myFunction);
spied('Labas');
spied('Cia as');
console.log(spied.report());