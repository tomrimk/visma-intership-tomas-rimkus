// Create two versions of a function called range which takes two numbers x and y and returns an array filled with all numbers from x (inclusive) to y (exclusive) 
// a) Do not use any ES6 methods 
// b) Use a suitable ES6 method 

function rangeNotES6(x, y) {
    if (x > y) return console.log('First number has to be lower than second');
    let arr = [];
    if (x === y) return [x]
    for (let i = x; i < y; i++) {
        arr.push(i);
    }
    console.log(arr);
}

const rangeES6 = (x, y) => {
    if (x > y) return console.log('First number has to be lower than second');

    x === y ? arr = [x] :
        console.log(
            new Array(y - x)
                .fill()
                .map((item, index) => index + x));
}

rangeES6(20, 18);
rangeNotES6(5, 10);