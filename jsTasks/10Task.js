// Given an array with nested arrays of numbers (ex.: [10, 6, [4, 8], 3, [6, 5, [9]]]) create a function that would sum all numbers from provided array. 

let arr = [10, 6, [4, 8], 3, [6, 5, [9]]];

const sumArray = arr => {
    var sum = 0;
    arr.forEach(element => {
        if (typeof element === "number") {
            sum += element;
        } else if (element instanceof Array) {
            sum += sumArray(element);
        }
    });
    
    return sum;
}

console.log(sumArray(arr));