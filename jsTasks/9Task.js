// Write a function sevenAte9 that removes each 9 that it is in between 7s. 
// sevenAte9('79712312') returns '7712312' 
// sevenAte9('79797') returns '777'

const sevenAte9 = givenNumber => console.log(givenNumber.replace(/797\d/g, '77'));

sevenAte9('79797');