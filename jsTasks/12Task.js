// Write the definition of the function "say" in such way that calling this: 
// say("Hello,")("it’s me"); Would return "Hello, it’s me"; 

const say = greeting => pronoun => console.log(`${greeting} ${pronoun}`);

say('Hello,')('it\'s me');