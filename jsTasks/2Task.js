// Write a program that uses console.log to print all the numbers from 1 to 100, with two exceptions. For numbers divisible by 3, print "Fizz" instead of the number, and for numbers divisible by 5 (and not 3), print "Buzz". For numbers that are divisible by both 3 and 5 print "FizzBuzz". 


for (let i = 1; i <= 100; i++) {
    let word = '';
    if (i % 3 === 0) {
        word += 'Fizz';
    }

    if (i % 5 === 0) {
        word += 'Buzz';
    }

    if (word === '') {
        word = i;
    }

    console.log(word);
}