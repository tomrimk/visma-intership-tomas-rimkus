// Create a function which takes an array and returns an array with all duplicates removed. 

let arr = [1, 1, 2, 3, 4, 3, 5, 2, 3, 7, 5, 8, 9, 4, 4, 5, 9, 2];
let stringArr = ['hey', 'hi', 'hello', 'hey', 'hi', 'good morning', 'hello'];

const removeDuplicates = (arr) => {
    return [...new Set(arr)];
}

console.log(removeDuplicates(arr));