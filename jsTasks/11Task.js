// Create two versions of a calculator module:  
// a) Do not use any ES6 functionality 
// b) Use a ES6 class 
// Calculator should have four methods: add, subtract, multiply and divide. All of calculator methods should be chainable. 
// Example: 
// var calc = new Calculator(0); 
// amount = calc.add(5).multiply(2).add(20).divide(3); should return 10

function Calculator() {
    this.answer = 0;
}

Calculator.prototype.add = function (num) {
    this.answer += num;
    return this;
}

Calculator.prototype.substract = function (num) {
    this.answer -= num;
    return this;
}

Calculator.prototype.multiply = function (num) {
    this.answer *= num;
    return this;
}

Calculator.prototype.divide = function (num) {
    this.answer /= num;
    return this;
}

class ES6Calculator {
    constructor(answer) {
        this.answer = answer;
    }

    add(num) {
        this.answer += num;
        return this;
    }

    substract(num) {
        this.answer -= num;
        return this;
    }

    multiply(num) {
        this.answer *= num;
        return this;
    }

    divide(num) {
        this.answer /= num;
        return this;
    }
}

let calc = new Calculator();
let amount = calc.add(5).multiply(2).add(20).divide(3).answer;

// let es6Calc = new ES6Calculator(0);
// let es6Amount = es6Calc.add(5).multiply(2).add(20).divide(3).answer;

console.log(amount);
// console.log(es6Amount);