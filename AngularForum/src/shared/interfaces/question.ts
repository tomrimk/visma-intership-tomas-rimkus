export interface IQuestion {
    title       : string,
    description : string,
    keywords    : string[],
    category    : string,
    createdAt   : Date,
    author      : IQuestionAuthor,
    stats       : IQuestionStats,
    id          : number
}

export interface IQuestionStats {
    views       : number,
    answers     : number,
    votes       : number
}

export interface IQuestionAuthor {
    name        : string,
    occupation  : string,
    imgUrl      : string
}