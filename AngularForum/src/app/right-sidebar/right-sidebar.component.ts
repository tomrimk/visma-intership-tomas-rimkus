import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'right-sidebar',
  templateUrl: './right-sidebar.component.html',
  styleUrls: ['./right-sidebar.component.scss']
})
export class RightSidebarComponent implements OnInit {
  @Input() questionsLength : number;
  @Input() membersCount    : number;

  constructor() { }

  ngOnInit() {
  }

}
