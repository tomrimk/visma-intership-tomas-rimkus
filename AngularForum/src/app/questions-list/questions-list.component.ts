import { Component, OnInit } from '@angular/core';

import { IQuestion } from '../../shared/interfaces/question';
import { QuestionsListService } from './questions-list.service';

@Component({
  selector: 'questions-list',
  templateUrl: './questions-list.component.html',
  styleUrls: ['./questions-list.component.scss']
})
export class QuestionsListComponent implements OnInit {
  questions    : IQuestion[];
  membersCount : number;

  constructor(private questionsService: QuestionsListService) {}

  ngOnInit() {
    this.getQuestions();
  }

  getQuestions(): void {
    this.questionsService.getQuestions().subscribe(questions => this.questions = questions.reverse())
  }

  getUniqueMembers(): number {
    let members = [];

    if(this.questions && this.questions.length) {
      this.questions.forEach(question => {
  
        const isMemberCounted = members.find(member => member === question.author.name);
  
        if (!isMemberCounted) {
          members = [...members, question.author.name];
        }
      });
    }

    return members.length;
  }
}
