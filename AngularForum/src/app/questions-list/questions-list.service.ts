import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import * as CONSTANTS from '../../shared/utils/constants';
import { IQuestion } from '../../shared/interfaces/question';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' }) 
export class QuestionsListService {
    private questionsUrl = `${CONSTANTS.BACKEND_URI}questions`;

    constructor(private http: HttpClient) {}

    getQuestions (): Observable<IQuestion[]> {
        return this.http.get<IQuestion[]>(this.questionsUrl);
    }

    getQuestion (id: number): Observable<IQuestion> {
        return this.http.get<IQuestion>(`${this.questionsUrl}/${id}`);
    }

    postQuestion (data: IQuestion): Observable<IQuestion> {
        return this.http.post<IQuestion>(this.questionsUrl, data, httpOptions);
    }

    editQuestion (data: IQuestion, id: number): Observable<IQuestion> {
        return this.http.put<IQuestion>(`${this.questionsUrl}/${id}`, data, httpOptions);
    }

    deleteQuestion (id: number): Observable<IQuestion> {
        return this.http.delete<IQuestion>(`${this.questionsUrl}/${id}`);
    }
}