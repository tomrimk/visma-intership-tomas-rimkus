import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './/app-routing.module';

import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { LeftSidebarComponent } from './left-sidebar/left-sidebar.component';
import { QuestionsListComponent } from './questions-list/questions-list.component';
import { RightSidebarComponent } from './right-sidebar/right-sidebar.component';
import { QuestionComponent } from './questions-list/question/question.component';
import { QuestionEditFormComponent } from './question-edit-form/question-edit-form.component';
import { SectionHeaderComponent } from './section-header/section-header.component';
import { FormComponentComponent } from "./question-edit-form/form-component/form-component.component";

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    LeftSidebarComponent,
    QuestionsListComponent,
    RightSidebarComponent,
    QuestionComponent,
    QuestionEditFormComponent,
    SectionHeaderComponent,
    FormComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
