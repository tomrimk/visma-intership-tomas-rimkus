import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { IQuestion } from '../../../shared/interfaces/question';
import { QuestionsListService } from '../../questions-list/questions-list.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form-component',
  templateUrl: './form-component.component.html',
  styleUrls: ['./form-component.component.scss']
})
export class FormComponentComponent implements OnInit, OnChanges{
  @Input() question    : IQuestion;
  @Input() toggleModal : Function;
  editForm             : FormGroup;

  id = +this.route.snapshot.paramMap.get('id');

  constructor(private formBuilder: FormBuilder, private questionsService: QuestionsListService, private router: Router, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.editForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      author: this.formBuilder.group({
        name: ['', Validators.required],
        occupation: ['', Validators.required],
        imgUrl: ['', Validators.required],
      }),
      keywords: ['', Validators.required],
      category: ['', Validators.required]
    });
  }

  toggle() {
    this.toggleModal();
  }

  fillForm() {
    if(this.question) {
      this.editForm.patchValue({
        title: this.question.title,
        description: this.question.description,
        author: {
          name: this.question.author.name,
          occupation: this.question.author.occupation,
          imgUrl: this.question.author.imgUrl,
        },
        keywords: this.question.keywords.join(' '),
        category: this.question.category
      });
    }
  }

  postQuestion(data: IQuestion): void {
    this.questionsService.postQuestion(data).subscribe(() => this.router.navigate(['/']));
  }

  editQuestion(data: IQuestion, id: number): void {
    this.questionsService.editQuestion(data, id).subscribe(() => this.router.navigate(['/']));
  }

  ngOnChanges() {
    this.fillForm();
  }

  onSubmit(): void{
    if(this.id) {
      let data = Object.assign({}, this.editForm.value, {
        keywords: this.editForm.value.keywords.split(' '),
        stats: this.question.stats,
        createdAt: this.question.createdAt
      });

      console.log(data);
      this.editQuestion(data, this.id);
      return;
    }

    let data = Object.assign({}, this.editForm.value, {
      stats: {
        views: 0,
        answers: 0,
        votes: 0
      },
      createdAt: new Date(),
      keywords: this.editForm.value.keywords.split(' ')
    });

    this.postQuestion(data);
  }
}
