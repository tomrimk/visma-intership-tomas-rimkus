import { Component, OnInit } from '@angular/core';
import { QuestionsListService } from '../questions-list/questions-list.service';
import { IQuestion } from '../../shared/interfaces/question';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-question-edit-form',
  templateUrl: './question-edit-form.component.html',
  styleUrls: ['./question-edit-form.component.scss']
})
export class QuestionEditFormComponent implements OnInit {
  question              : IQuestion;
  title                 : string;
  public bindedCallback : Function;
  openModal             : boolean;

  id = +this.route.snapshot.paramMap.get('id');

  constructor(private questionsService: QuestionsListService,private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getQuestionFromBackend();
    this.openModal = false;
    this.bindedCallback = this.toggleModal.bind(this);
  }

  public toggleModal() {
    this.openModal = !this.openModal;
  }
  
  getQuestion(): IQuestion {
    return this.question;
  }

  deleteQuestion() {
    this.questionsService.deleteQuestion(this.id).subscribe(() => this.router.navigate(['/']));
  }

  setQuestion(question) : void {
    this.question = question;
  }

  getQuestionFromBackend() {
    if(this.id) {
      this.title = "Edit Question";
      this.questionsService.getQuestion(this.id).subscribe(question => this.setQuestion(question));

      return;
    }

    this.title = "New Question";
  }
}
