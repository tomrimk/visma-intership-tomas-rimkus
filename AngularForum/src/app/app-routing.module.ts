import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { QuestionEditFormComponent } from './question-edit-form/question-edit-form.component';
import { QuestionsListComponent } from './questions-list/questions-list.component';

const routes: Routes = [
  { path: '', component: QuestionsListComponent },
  { path: 'edit/:id', component: QuestionEditFormComponent},
  { path: 'create', component: QuestionEditFormComponent}
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [ RouterModule.forRoot(routes) ],
})
export class AppRoutingModule {
  
}
